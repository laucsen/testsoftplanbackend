Softplan Challenge - Backend
===================

## Configuração

### Requerimentos

1. Docker operacional
2. NodeJs e NPM operacional

### Como rodar

1. Rode a imagem docker base de dados fornecida pelo teste: `docker run -d -p 5432:5432 justiandre/test-temp-pj-spec-gestao-processos-db:latest`
2. Construa a imagem: `docker build -t softplan-backend .`
3. Rode a imagem do `backend`: `docker run -d -p 7070:7070 softplan-backend`

#### Observações

1. O `backend` ficará disponível na porta: `7070`

## Observações do Teste

1. Foi realizada a modelagem apenas da entidade responsável dada a necessidade do teste.
2. Não foi realizado o teste de validação fornecido pelo teste.
3. Não foi implementado os erros de NOT FOUND como descrito no teste para as ações de edição e deleção.

## O que poderia melhorar

1. Realizar o teste de validação fornecido pelo teste, paraque todos os casos passem com exatidão.
2. Implementar os erros de NOT FOUND como descrito no teste para as ações de edição e deleção.