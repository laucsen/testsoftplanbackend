const handleNotFound = (data) => {
  if (data === null) {
    throw new Error('Entity not found');
  }
  return data;
};

const handleBadRequest = (response) => (err) => {
  response.status(400);
  return response.json({ error: 'Bad Request', error_description: err.message });
};

const handleErrors = (response) => (err) => {
  response.status(404);
  response.json({ error: 'Not Found', error_description: err.message });
};

const handleNewEntity = (response) => (data) => {
  response.status(201);
  response.json(data);
};

const handlePreconditionFailed = (response) => (data) => {
  response.status(412);
  response.json(data);
};

export {
  handleNotFound,
  handleBadRequest,
  handleErrors,
  handleNewEntity,
  handlePreconditionFailed,
};
