import { Router } from 'express';

import { initialize } from './model';
import {
  findAll,
  create,
  get,
  edit,
  del,
} from './controller';

export default ({ db }) => {
  const responsibleModel = initialize({ db });

  const router = Router();

  router.post('/find-all/', findAll({ responsibleModel }));
  router.post('/', create({ responsibleModel }));
  router.get('/:id/', get({ responsibleModel }));
  router.put('/:id/', edit({ responsibleModel }));
  router.delete('/:id/', del({ responsibleModel }));

  return router;
};
