import Sequelize from 'sequelize';

import {
  handleNewEntity,
  handlePreconditionFailed,
  handleErrors,
  handleBadRequest,
  handleNotFound,
} from '../handlers';
import translateErrors from './errors';

const { Op } = Sequelize;

const paginate = (items, size, page) => {
  const sizeN = parseInt(size);
  const pageN = parseInt(page);

  const position = pageN * sizeN;
  return items.slice(position, position + sizeN);
};

/**
 * Service to get all responsibles ... /TODO
 */
const findAll = ({ responsibleModel }) => (req, res) => {
  const { body, query } = req;

  const { nome, email, cpf } = body;
  const { page = 0, size = 10 } = query;

  const filter = {};
  if (nome !== undefined) {
    filter.nome = { [Op.iLike]: `%${nome}%` };
  }
  if (email !== undefined) {
    filter.email = { [Op.iLike]: `%${email}%` };
  }
  if (cpf !== undefined) {
    const cleanCpf = cpf.replace(/[.-]/gm, '');
    filter.cpf = { [Op.like]: `%${cleanCpf}%` };
  }

  responsibleModel
    .findAll({
      where: {
        ...filter,
      },
    })
    .then((items) => {
      return {
        records: paginate(items, size, page),
        records_number: items.length,
      };
    })
    .then((records) => res.json(records));
};

/**
 * Function to create a custom ID.
 * This case, if two `responsible`is created at same time, the id may conflict.
 * A good fix to this case is add autoincrement to `id` on postgresql database.
 * @returns {number}
 */
const genID = () => {
  return ((new Date(Date.now())).getTime() + Math.floor(Math.random() * 100000));
};

/**
 * Create a new Responsible
 */
const create = ({ responsibleModel }) => (req, res) => {
  const { body } = req;

  responsibleModel
    .create({
      ...body,
      id: genID(),
    })
    .then(handleNewEntity(res))
    .catch((err) => handlePreconditionFailed(res)(translateErrors(err)));
};

/**
 * Get single responsible by id
 */
const get = ({ responsibleModel }) => (req, res) => {
  const { id } = req.params;

  responsibleModel
    .findByPk(id)
    .then(handleNotFound)
    .then((responsible) => res.json(responsible))
    .catch(handleErrors(res));
};

/**
 * Edit single entity by its ID.
 */
const edit = ({ responsibleModel }) => (req, res) => {
  const { id } = req.params;
  const { body } = req;

  if (parseInt(id) !== parseInt(body.id)) {
    return handleBadRequest(res)(new Error('Invalid Entity'));
  }

  return responsibleModel
    .findByPk(id)
    .then((responsible) => {
      responsible.nome = body.nome;
      responsible.cpf = body.cpf;
      responsible.email = body.email;
      responsible.data_nascimento = body.data_nascimento;

      return responsible.save();
    })
    .then((responsible) => res.json(responsible))
    .catch((err) => handlePreconditionFailed(res)(translateErrors(err)));
};

const del = ({ responsibleModel }) => (req, res) => {
  const { id } = req.params;

  responsibleModel
    .findByPk(id)
    .then((responsible) => {
      return responsible.destroy();
    })
    .then((responsible) => res.json(responsible))
    .catch((err) => handlePreconditionFailed(res)(translateErrors(err)));
};

export {
  findAll,
  create,
  get,
  edit,
  del,
};
