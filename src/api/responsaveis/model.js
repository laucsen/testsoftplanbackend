import Sequelize from 'sequelize';
import moment from 'moment';

let Responsible;

const initialize = ({ db }) => {
  Responsible = db.define('Responsible',
    {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      nome: {
        type: Sequelize.STRING(75),
        allowNull: false,
        validate: {
          len: [0, 75],
        },
      },
      cpf: {
        type: Sequelize.STRING(11),
        unique: true,
        allowNull: false,
        validate: {
          cpf: (value) => {
            if (value.length !== 11 || isNaN(value)) {
              throw new Error('Cpf error');
            }
          },
        },
      },
      email: {
        type: Sequelize.STRING(200),
        unique: true,
        allowNull: false,
        validate: {
          len: [0, 65],
          isEmail: true,
        },
      },
      data_nascimento: {
        type: Sequelize.DATE,
        validate: {
          menorDataAtual: (value) => {
            const now = moment().endOf('day').toDate();
            if (new Date(value) > now) {
              throw new Error('Date error');
            }
          },
        },
      },
    }, {
      // define the table's name
      tableName: 'responsavel',
    });

  return Responsible;
};

export {
  initialize,
  Responsible as default,
};
