const truePath = {
  'data_nascimento': 'dataNascimento',
};

const errorType = {
  'is_null': 'obrigatorio',
  'len': 'tamanhoMaximo',
  'cpf': 'invalido',
  'not_unique': 'duplicado',
  'isEmail': 'invalido',
  'menorDataAtual': 'menorDataAtual',
};

const names = {
  'email': 'e-mail',
  'cpf': 'CPF',
  'nome': 'nome',
};

const messages = {
  'is_null': (path) => (`O ${names[path]} é obrigatório`),
  'len': (path) => (`O ${names[path]} deve possuir no máximo {} caracteres`),
  'cpf': () => ('O CPF é inválido, o CPF deve possuir exatamente {} caracteres numéricos'),
  'not_unique': (path) => (`O ${names[path]} é duplicado, já existe outro responsável com o ${names[path]} informado`),
  'isEmail': () => ('O e-mail informado é inválido'),
  'menorDataAtual': () => ('A data de nascimento deve ser menor que a data atual'),
};

const args = {
  'is_null': () => ([]),
  'len': (validatorArgs) => ([validatorArgs[1]]),
  'cpf': () => ([11]),
  'not_unique': () => ([]),
  'isEmail': () => ([]),
  'menorDataAtual': () => ([]),
};

const translateErrors = (errException) => {
  return errException.errors.map((err) => {
    const { validatorKey, validatorArgs } = err;

    return {
      local_erro: `responsavel.${truePath[err.path] || err.path}`,
      erro: errorType[validatorKey],
      mensagem: messages[validatorKey](err.path),
      args: args[validatorKey](validatorArgs),
    };
  });
};

export {
  translateErrors as default,
};
