import { Router } from 'express';
import { version } from '../../package.json';

import responsaveis from './responsaveis';

export default ({ config, db }) => {
  const api = Router();

  // mount services
  api.use('/responsaveis', responsaveis({ config, db }));

  // perhaps expose some API metadata at the root
  api.get('/', (req, res) => {
    res.json({ version });
  });

  return api;
};
