/* eslint-disable */

import Sequelize from 'sequelize';

import config from './config';

const initialize = async () => {
  console.log('Starting Database');

  const sequelizeInstance = new Sequelize(config.databaseName, config.databaseUser, config.databasePass, {
    dialect: 'postgres',
    host: config.databaseIP,
    port: config.databasePort,
    operatorsAliases: false,
    define: {
      timestamps: false // true by default
    },

    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  });

  return new Promise((resolve, reject) => {
    return sequelizeInstance
      .authenticate()
      .then(() => {
        console.log('Connection has been established successfully with Database.');
        resolve(sequelizeInstance);
      })
      .catch(err => {
        console.error('Unable to connect to the database:', err);
        reject(err);
      });
  })
};

export {
  initialize as default,
};
