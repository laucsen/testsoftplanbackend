/* eslint-disable */

import express from 'express';
import bodyParser from 'body-parser';

import api from '../../src/api';
import config from '../../src/config.json';
import initializeDb from '../../src/db';

let db;

const beforeAction = async () => {
  let app = express();

  app.use(bodyParser.json({
    limit: config.bodyLimit
  }));

  // connect to db
  db = await initializeDb();

  // api router
  app.use('/api', api({ config, db }));

  return app;
};

const afterAction = async () => {
  // await db.close();
};


module.exports = { beforeAction, afterAction };
