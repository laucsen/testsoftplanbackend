import request from 'supertest';
import { expect } from 'chai';

const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');
const {
  afterSamples,
} = require('../setup/samples');

let app;

describe('Responsible CRUD - PUT (EDIT)', () => {
  before(async () => {
    app = await beforeAction();
  });

  after(async () => {
    await afterAction();
  });

  afterEach(async () => {
    await afterSamples();
  });

  let createdId;
  let sampleResponsible;

  beforeEach((done) => {
    request(app)
      .post('/api/responsaveis/')
      .send({
        cpf: '38428401012',
        data_nascimento: new Date(1984, 0, 4),
        email: 'seo@gmail.com',
        nome: 'Someone Elswhere Somewhere',
      })
      .expect(201)
      .end((err, res) => {
        createdId = res.body.id;
        expect(res.status).to.be.equal(201);
        done(err);
      });
  });

  beforeEach((done) => {
    request(app)
      .get(`/api/responsaveis/${createdId}`)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);
        sampleResponsible = res.body;
        done(err);
      });
  });


  it('it should edit a single responsible', (done) => {
    request(app)
      .put(`/api/responsaveis/${sampleResponsible.id}`)
      .send({
        ...sampleResponsible,
        nome: 'Someone Somewhere',
      })
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);
        expect(res.body.nome).to.be.equal('Someone Somewhere');
        done(err);
      });
  });

  it('it should edit a single responsible with validations rules', (done) => {
    request(app)
      .put(`/api/responsaveis/${sampleResponsible.id}`)
      .send({
        ...sampleResponsible,
        nome: 'Someone Somewhere Somewhere Somewhere Somewhere Somewhere Somewhere Somewhere Somewhere',
      })
      .expect(412)
      .end((err, res) => {
        expect(res.status).to.be.equal(412);
        expect(res.body).to.deep.equal([{
          erro: 'tamanhoMaximo',
          local_erro: 'responsavel.nome',
          mensagem: 'O nome deve possuir no máximo {} caracteres',
          args: [75],
        }]);
        done(err);
      });
  });
});
