import request from 'supertest';
import { expect } from 'chai';

const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');
const {
  afterSamples,
} = require('../setup/samples');

let app;

describe('Responsible CRUD - GET (find by id)', () => {
  before(async () => {
    app = await beforeAction();
  });

  after(async () => {
    await afterAction();
  });

  afterEach(async () => {
    await afterSamples();
  });

  let createdId;

  beforeEach((done) => {
    request(app)
      .post('/api/responsaveis/')
      .send({
        cpf: '38428401012',
        data_nascimento: new Date(1984, 0, 4),
        email: 'seo@gmail.com',
        nome: 'Someone Elswhere Somewhere',
      })
      .expect(201)
      .end((err, res) => {
        createdId = res.body.id;
        expect(res.status).to.be.equal(201);
        done(err);
      });
  });

  it('it should get a single responsible by its ID', (done) => {
    request(app)
      .get(`/api/responsaveis/${createdId}`)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);

        expect(res.body).to.deep.equal({
          cpf: '38428401012',
          data_nascimento: '1984-01-04',
          email: 'seo@gmail.com',
          id: createdId,
          nome: 'Someone Elswhere Somewhere',
        });

        done(err);
      });
  });
});
