import request from 'supertest';
import { expect } from 'chai';

const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');
const {
  afterSamples,
} = require('../setup/samples');

let app;

describe('Responsible CRUD - GET ALL (find-all)', () => {
  before(async () => {
    app = await beforeAction();
  });

  after(async () => {
    await afterAction();
  });

  afterEach(async () => {
    await afterSamples();
  });

  const build = async (item) => {
    return request(app)
      .post('/api/responsaveis/')
      .send(item)
      .expect(201);
  };

  beforeEach(async () => {
    await build({
      cpf: '38428401012',
      data_nascimento: new Date(1984, 0, 4),
      email: 'seo@gmail.com',
      nome: 'Someone Elswhere Somewhere',
    });
    await build({
      cpf: '52283871026',
      data_nascimento: new Date(1954, 0, 4),
      email: 'seo1@mtv.com',
      nome: 'Someone Elswhere Somewhere 1',
    });
    await build({
      cpf: '93633129090',
      data_nascimento: new Date(1974, 0, 4),
      email: 'seo2@gmail.com',
      nome: 'Someone Somewhere 2',
    });
    await build({
      cpf: '15317672066',
      data_nascimento: new Date(1944, 0, 4),
      email: 'seo3@yahoo.com',
      nome: 'Someone Elswhere Somewhere 3',
    });
    await build({
      cpf: '72816780023',
      data_nascimento: new Date(1944, 0, 4),
      email: 'seo4@gmail.com',
      nome: 'Someone Elswhere Somewhere 4',
    });
    await build({
      cpf: '53266285034',
      data_nascimento: new Date(1944, 0, 4),
      email: 'seo5@gmail.com',
      nome: 'Someone Elswhere Somewhere 5',
    });
    await build({
      cpf: '77739840009',
      data_nascimento: new Date(1944, 0, 4),
      email: 'seo6@gmail.com',
      nome: 'Someone Elswhere Somewhere 6',
    });
  });

  it('it should find-all responsibles', (done) => {
    request(app)
      .post('/api/responsaveis/find-all')
      .send({})
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);

        expect(res.body.records.length).to.be.equal(7);
        expect(res.body.records_number).to.be.equal(7);

        done(err);
      });
  });

  it('should filter by name', (done) => {
    request(app)
      .post('/api/responsaveis/find-all')
      .send({
        nome: 'somewhere 6',
      })
      .expect(200)
      .end((err, { status, body }) => {
        expect(status).to.be.equal(200);

        expect(body.records.length).to.be.equal(1);
        expect(body.records[0].nome).to.be.equal('Someone Elswhere Somewhere 6');

        done(err);
      });
  });

  it('should filter by email', (done) => {
    request(app)
      .post('/api/responsaveis/find-all')
      .send({
        email: '@gmail.com',
      })
      .expect(200)
      .end((err, { status, body }) => {
        expect(status).to.be.equal(200);

        expect(body.records.length).to.be.equal(5);

        done(err);
      });
  });

  it('should filter by cpf', (done) => {
    request(app)
      .post('/api/responsaveis/find-all')
      .send({
        cpf: '153.1',
      })
      .expect(200)
      .end((err, { status, body }) => {
        expect(status).to.be.equal(200);

        expect(body.records.length).to.be.equal(1);

        done(err);
      });
  });

  it('should filter by multiple fields', (done) => {
    request(app)
      .post('/api/responsaveis/find-all')
      .send({
        email: '@gmail.com',
        nome: 'elswher',
      })
      .expect(200)
      .end((err, { status, body }) => {
        expect(status).to.be.equal(200);

        expect(body.records.length).to.be.equal(4);

        done(err);
      });
  });

  it('it should find-all responsibles with pagination 1', (done) => {
    request(app)
      .post('/api/responsaveis/find-all?page=0&size=3')
      .send({})
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);

        expect(res.body.records.length).to.be.equal(3);
        expect(res.body.records_number).to.be.equal(7);

        done(err);
      });
  });

  it('it should find-all responsibles with pagination 2', (done) => {
    request(app)
      .post('/api/responsaveis/find-all?page=1&size=3')
      .send({})
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);

        expect(res.body.records.length).to.be.equal(3);
        expect(res.body.records_number).to.be.equal(7);

        done(err);
      });
  });

  it('it should find-all responsibles with pagination 3', (done) => {
    request(app)
      .post('/api/responsaveis/find-all?page=2&size=3')
      .send({})
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.be.equal(200);

        expect(res.body.records.length).to.be.equal(1);
        expect(res.body.records_number).to.be.equal(7);

        done(err);
      });
  });
});
