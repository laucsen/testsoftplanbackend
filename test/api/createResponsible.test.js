import request from 'supertest';
import { expect } from 'chai';

const {
  beforeAction,
  afterAction,
} = require('../setup/_setup');
const {
  afterSamples,
} = require('../setup/samples');

let app;

describe('Responsible CRUD - POST (CREATE)', () => {
  before(async () => {
    app = await beforeAction();
  });

  after(async () => {
    await afterAction();
  });

  afterEach(async () => {
    await afterSamples();
  });

  it('it should crete a responsible on POST', (done) => {
    request(app)
      .post('/api/responsaveis/')
      .send({
        cpf: '38428401012',
        data_nascimento: new Date(1984, 0, 4),
        email: 'seo@gmail.com',
        nome: 'Someone Elswhere Somewhere',
      })
      .expect(201)
      .end((err, res) => {
        expect(res.status).to.be.equal(201);

        expect(res.body.email).to.be.equal('seo@gmail.com');

        done();
      });
  });

  describe('Validation Errors', () => {
    // => GENERAL VALIDATIONS
    describe('General Validations', () => {
      it('it should test for mandatory fields', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            data_nascimento: new Date(1984, 0, 4),
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(412);

            const { body } = res;

            expect(body.length).to.be.equal(3);

            expect(body[0]).to.deep.equal({
              local_erro: 'responsavel.nome',
              erro: 'obrigatorio',
              mensagem: 'O nome é obrigatório',
              args: [],
            });
            expect(body[1]).to.deep.equal({
              local_erro: 'responsavel.cpf',
              erro: 'obrigatorio',
              mensagem: 'O CPF é obrigatório',
              args: [],
            });
            expect(body[2]).to.deep.equal({
              local_erro: 'responsavel.email',
              erro: 'obrigatorio',
              mensagem: 'O e-mail é obrigatório',
              args: [],
            });

            done();
          });
      });

      it('it should test fields max length', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            nome: 'Someone Elswhere Somewhere Somewhere Jr. Somewhere Son. Somewheresson Ragnarsson',
            cpf: '38428401012',
            email: 'seoElswheresomewheresomewherejr.somewhereson.somewheresson@gmail.com',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(412);

            const { body } = res;

            expect(body.length).to.be.equal(2);

            expect(body[0]).to.deep.equal({
              local_erro: 'responsavel.nome',
              erro: 'tamanhoMaximo',
              mensagem: 'O nome deve possuir no máximo {} caracteres',
              args: [75],
            });

            expect(body[1]).to.deep.equal({
              local_erro: 'responsavel.email',
              erro: 'tamanhoMaximo',
              mensagem: 'O e-mail deve possuir no máximo {} caracteres',
              args: [65],
            });

            done();
          });
      });
    });

    // => data_nascimento SPECIAL VALIDATIONS
    it('it should test if birth date is smaller than today', (done) => {
      const now = new Date(Date.now());
      const tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 10);

      request(app)
        .post('/api/responsaveis/')
        .send({
          cpf: '38428401012',
          data_nascimento: tomorrow,
          email: 'seo@gmail.com',
          nome: 'Someone Elswhere Somewhere',
        })
        .expect(201)
        .end((err, res) => {
          expect(res.status).to.be.equal(412);

          expect(res.body[0]).to.deep.equal({
            local_erro: 'responsavel.dataNascimento',
            erro: 'menorDataAtual',
            mensagem: 'A data de nascimento deve ser menor que a data atual',
            args: [],
          });

          done();
        });
    });

    // => EMAIL SPECIAL VALIDATIONS
    describe('email special validations', () => {
      it('it should validate email structure', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            cpf: '38428401012',
            data_nascimento: new Date(1984, 0, 4),
            email: 'seo@gmail.com@empire',
            nome: 'Someone Elswhere Somewhere',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(412);

            expect(res.body[0]).to.deep.equal({
              local_erro: 'responsavel.email',
              erro: 'invalido',
              mensagem: 'O e-mail informado é inválido',
              args: [],
            });

            done();
          });
      });
    });

    // => CPF SPECIAL VALIDATIONS
    describe('cpf special validations', () => {
      it('it should validate cpf max length', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            nome: 'Someone Elsewhere Somewhere',
            cpf: '384284010120',
            email: 'seo@gmail.com',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(412);

            const { body } = res;

            expect(body[0]).to.deep.equal({
              local_erro: 'responsavel.cpf',
              erro: 'invalido',
              mensagem: 'O CPF é inválido, o CPF deve possuir exatamente {} caracteres numéricos',
              args: [11],
            });

            done();
          });
      });

      it('it should validate cpf minimum length', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            nome: 'Someone Elsewhere Somewhere',
            cpf: '3842840101',
            email: 'seo@gmail.com',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(412);

            const { body } = res;

            expect(body[0]).to.deep.equal({
              local_erro: 'responsavel.cpf',
              erro: 'invalido',
              mensagem: 'O CPF é inválido, o CPF deve possuir exatamente {} caracteres numéricos',
              args: [11],
            });

            done();
          });
      });

      it('it should validate cpf numbers', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            nome: 'Someone Elsewhere Somewhere',
            cpf: '3842840101P',
            email: 'seo@gmail.com',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(412);

            const { body } = res;

            expect(body[0]).to.deep.equal({
              local_erro: 'responsavel.cpf',
              erro: 'invalido',
              mensagem: 'O CPF é inválido, o CPF deve possuir exatamente {} caracteres numéricos',
              args: [11],
            });

            done();
          });
      });
    });

    // => Duplication Errors
    describe('Duplication Errors', () => {
      it('it should not repeat CPF', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            nome: 'Someone Elsewhere Somewhere',
            cpf: '88220471093',
            email: 'seo@gmail.com',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(201);
            return request(app)
              .post('/api/responsaveis/')
              .send({
                nome: 'Someone Somewhere',
                cpf: '88220471093',
                email: 'seo2@gmail.com',
              })
              .expect(201)
              .end((err2, res2) => {
                expect(res2.status).to.be.equal(412);

                const { body } = res2;

                expect(body.length).to.deep.equal(1);

                expect(body[0]).to.deep.equal({
                  local_erro: 'responsavel.cpf',
                  erro: 'duplicado',
                  mensagem: 'O CPF é duplicado, já existe outro responsável com o CPF informado',
                  args: [],
                });

                done();
              });
          });
      });

      it('it should not repeat EMAIL', (done) => {
        request(app)
          .post('/api/responsaveis/')
          .send({
            nome: 'Someone Elsewhere Somewhere',
            cpf: '88220471093',
            email: 'seo@gmail.com',
          })
          .expect(201)
          .end((err, res) => {
            expect(res.status).to.be.equal(201);
            return request(app)
              .post('/api/responsaveis/')
              .send({
                nome: 'Someone Somewhere',
                cpf: '22344756000',
                email: 'seo@gmail.com',
              })
              .expect(201)
              .end((err2, res2) => {
                expect(res2.status).to.be.equal(412);

                const { body } = res2;

                expect(body.length).to.deep.equal(1);

                expect(body[0]).to.deep.equal({
                  local_erro: 'responsavel.email',
                  erro: 'duplicado',
                  mensagem: 'O e-mail é duplicado, já existe outro responsável com o e-mail informado',
                  args: [],
                });

                done();
              });
          });
      });
    });
  });
});
